const images = [
    {
        src: 'style/pictures/amaz2.png',
        category: 'Graphic Design',
        class:''
    },    {
        src: 'style/pictures/amaz3.png',
        category: 'Web Design'
    },    {
        src: 'style/pictures/amaz4.png',
        category: 'Landing Pages'
    },    {
        src: 'style/pictures/amaz5.png',
        category: 'Wordpress'
    },    {
        src: 'style/pictures/amaz6.png',
        category: 'Graphic Design'
    },    {
        src: 'style/pictures/amaz7.png',
        category: 'Wordpress'
    },    {
        src: 'style/pictures/amaz8.png',
        category: 'Landing Pages'
    },    {
        src: 'style/pictures/amaz9.png',
        category: 'Landing Pages'
    },    {
        src: 'style/pictures/amaz10.png',
        category: 'Wordpress'
    },{
        src: 'style/pictures/amaz11.png',
        category: 'Graphic Design'
    },{
        src: 'style/pictures/amaz12.png',
        category: 'Graphic Design'
    },{
        src: 'style/pictures/amaz2.png',
        category: 'Landing Pages'
    },{
        src: 'style/pictures/amaz2.png',
        category: 'Graphic Design'
    },{
        src: 'style/pictures/amaz2.png',
        category: 'Wordpress'
    },{
        src: 'style/pictures/amaz3.png',
        category: 'Web Design'
    },{
        src: 'style/pictures/amaz11.png',
        category: 'Web Design'
    },{
        src: 'style/pictures/amaz11.png',
        category: 'Web Design'
    },{
        src: 'style/pictures/amaz10.png',
        category: 'Web Design'
    },{
        src: 'style/pictures/amaz4.png',
        category: 'Landing Pages'
    },{
        src: 'style/pictures/amaz10.png',
        category: 'Wordpress'
    },{
        src: 'style/pictures/amaz10.png',
        category: 'Web Design'
    },{
        src: 'style/pictures/amaz6.png',
        category: 'Web Design'
    },{
        src: 'style/pictures/amaz4.png',
        category: 'Landing Pages'
    },{
        src: 'style/pictures/amaz4.png',
        category: 'Landing Pages'
    },
]

let perPage = 12;
let currentCategory=`all`;

const render = (arr) => {
    const mainContainer = document.querySelector('.grid');
    const sliceArray = arr.slice(0, perPage);
    const htmlArray = sliceArray.map((element) => {
        return `<img class="grid-item-img" src="${element.src}">`
    });
    mainContainer.innerHTML = htmlArray.join(' ');
    const btn = document.querySelector('.load-more');

    if (perPage >= arr.length) {
        btn.classList.add(`visible`);
    } else {
        btn.classList.remove(`visible`);
    };
}
render(images);

const filteredArr = (arr, category) => {
    return arr.filter((e)=> e.category === category)
}

console.log(filteredArr(images))

const tabsContainer = document.querySelector('.tabs');
tabsContainer.addEventListener('click', (event) => {
    const category = event.target.dataset.category;
    currentCategory = category;
    perPage = 12;
    if (category === "all" && event.target !== event.currentTarget) {
        render(images);
    } else if (event.target !== event.currentTarget) {
        const newArr = filteredArr(images, category);
        render(newArr);}
    const tabs = document.querySelectorAll('.tab');
    tabs.forEach(e => {
        e.classList.remove('active');
    });
    event.target.classList.add('active');
});

const button = document.querySelector(".load-more");
button.addEventListener(`click`, () => {
    perPage = perPage+12;
    if(currentCategory===`all`){
        render(images);
    }else{
        render(filteredArr (images, currentCategory));
    }
})




const tabs = document.querySelector('.service-menu')
const tabText = Array.from(document.querySelectorAll('.service-menu-information > li'))
//
console.log(tabText)
tabs.addEventListener('click',(event)=>{
    event.target.closest('.service-menu').querySelector('.active').classList.remove('active')
//    пойди к родителю найти эл с  ектив и удали класс ектив у этих эл а вот этому где произошло событие добавь ектив
    event.target.classList.add('active')
    const tabData = event.target.dataset.tab

    tabText.forEach((item)=>{
        console.log(item)
        item.setAttribute('hidden','true')
        if (item.dataset.text === tabData){
            item.removeAttribute('hidden')
        }

    })
})





