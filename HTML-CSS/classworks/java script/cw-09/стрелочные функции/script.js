function sum1(a,b){
    return a+b;
}

const sum2 = function (a,b){
    return a + b;
}

const sum3 = (a,b) => {
    return a + b;
}

const sum4 = (a,b) => a + b;

const hello = () =>console.log('Hello');

const hello2= () => {
    console.log('Hello');
    console.log('Hello,again');
};
