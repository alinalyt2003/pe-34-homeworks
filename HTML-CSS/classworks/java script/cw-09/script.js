// Объявить переменную "number" и присвоить ей значение 16
//
let number
number = 16;

// 2. Вывести в консоль 'Четное', если в переменной number содержится четное число,
// или 'Нечетное' в противном случае.
if (number % 2 === 0) {
    console.log('Четное')
} else if (number % 2 !== 0){
    console.log('Нечетное')
}
// 3. Дан массив. Вывести в консоль все элементы массива используя цикл for
const array = ['Alice', 'Bob', 'Charlie', 'Dave', 'Eve', 'Frank'];
for (let i = 0; i < array.length; i++){
 console.log(array[i]);
}
// 4. Вывести в консоль все элементы этого же массива используя цикл for .. of
for( let person of array){
    console.log(person);
}

// 5. Написать функцию, которая:
// - принимает два аргумента
// - возвращает их сумму
// Вызвать эту функцию и вывести результат в консоль.
function sum(a, b) {
    return a + b;
}
const sum2 = (a, b) => a + b;
const num = sum(5, 10);
console.log(num);
console.log(sum2(5, 10));
// 6. Дан объект. Нужно:
// - добавить объекту новое свойство "lastName" со значением "Smith"
// - удалить свойство firstName
const obj = {
    firstName: 'Alice'
};
obj['lastName'] = 'Smith' // obj.lastName = 'Smith'
delete obj.firstName;

// 7. Дана строка. Вывести в консоль часть этой строки чтобы получилось 'weis'.
const str = 'Budweiser';
console.log(str.slice(3,-2));
