/**
 * Задание 1.
 *
 * Написать имплементацию встроенной функции строки repeat(times).
 *
 * Функция должна обладать двумя параметрами:
 * - Целевая строка для повторения;
 * - Количество повторений целевой строки.
 *
 * Функция должна возвращать преобразованную строку.
 *
 * Условия:
 * - Генерировать ошибку, если первый параметр не является строкой,
 * а второй не является числом.
 */
/* Решение */
function repeat (str, qty){
    if(typeof str !== 'string'){
        throw Error('First param is not a string');
    }
    if(isNaN(qty)){
        throw Error('First param is not a number type.');
    }
    let result ='';
    for (let i = 0; i < qty; i++){
        result = result + str
    }
    return result
}
console.log(repeat)


/* Пример */
// const string = 'Hello, world!';
// console.log(repeat(string, 3)); // Hello, world!Hello, world!Hello, world!
// console.log(repeat(string, 1)); // Hello, world!
// console.log(repeat(string, 2)); // Hello, world!Hello, world!
// console.log(repeat(string, 5)); // Hello, world!Hello, world!Hello, world!Hello, world!Hello, world!
// console.log(repeat(7, 5)); // Error: First parameter should be a string type.