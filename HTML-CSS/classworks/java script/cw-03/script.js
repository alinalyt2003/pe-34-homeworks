// Код Math.floor(Math.random() * 6) + 1 позволяет загадать случайное число от 1 до 6.
// Напишите программу, которая загадывает такое случайное число, а потом просит пользователя его угадать (С помощью prompt).
// Если пользователь угадал число, необходимо вывести сообщение Поздравляем!. Если загаданное число
// меньше того, что ввел пользователь, необходимо вывести сообщение Меньше!. Если наоборот, вывести сообщение Больше!

// let randomNumber = Math.floor(Math.random()*6) + 1;
//
// let answer = prompt("Угадайте число");
//
// if(randomNumber===answer) {
//     console.log("Число правильное");
// }
// else if(randomNumber<answer){
//     console.log("Загаданное число больше")
// }
// else{
//     console.log("Загаданное число меньше")
// }
// console.log(randomNumber);

// /**
//  * Задание 2.
//  *
//  * Написать программу, которая будет приветствовать пользователя.
//  * Сперва пользователь вводит своё имя, после чего программа выводит в консоль сообщение с учётом его должности.
//  *
//  * Список должностей:
//  * Mike — CEO;
//  * Jane — CTO;
//  * Walter — программист:
//  * Oliver — менеджер;
//  * John — уборщик.
//  *
//  * Если введёно не известное программе имя — вывести в консоль сообщение «Пользователь не найден.».
//  *
//  * Выполнить задачу в двух вариантах:
//  * - используя конструкцию if/else if/else;
//  * - используя конструкцию switch.
//  */

// const userName = prompt('Введите имя пользователя');
// let pos = ''
// switch (userName.toLowerCase()) {
//     case'Mike':
//         pos="CEO";
//         console.log(`Привет $(userName) -${pos}`)
//         break;
//     case 'Jane':
//         pos="СТО";
//         console.log(`Привет $(userName) -${pos}`)
//         break;
//     case'Walter':
//         pos='Программист';
//         console.log(`Привет $(userName) -${pos}`)
//         break;
//     case 'Oliver':
//         pos='менеджеp';
//         console.log(`Привет $(userName) -${pos}`)
//         break;
//     case'john':
//         pos='Уборщик';
//         console.log(`Привет $(userName) -${pos}`)
//         break;
//
// }


//
// const userName =prompt('Enter Name');
//
// if (userName==='Mike'){
//     console.log ("Hello, CEO" + userName);
// }
//  else if (userName==='Jane'){
//      console.log("Hello, CEO" +userName);
//  }
//
// else if (userName==='Oliver'){
//     console.log("Hello, менеджер" +userName);
// }
//  else if (userName==='John'){
//     console.log("Hello, уборщик" +userName);
// }
//  else {
//      console.log ("User Unknown")
// }

// /**
//  * Задание 4.
//  *
//  * Напишите программу «Кофейная машина».
//  *
//  * Программа принимает монеты и готовит напитки:
//  * - Кофе за 25 монет;
//  * - Капучино за 50 монет;
//  * - Чай за 10 монет.
//  *
//  * Чтобы программа узнала что делать, она должна знать:
//  * - Сколько монет пользователь внёс;
//  * - Какой он желает напиток.
//  *
//  * В зависимости от того, какой напиток выбрал пользователь,
//  * программа должна вычислить сдачу и вывести сообщение в консоль:
//  * «Ваш «НАЗВАНИЕ НАПИТКА» готов. Возьмите сдачу: «СУММА СДАЧИ».".
//  *
//  * Если пользователь ввёл сумму без сдачи — вывести сообщение:
//  * «Ваш «НАЗВАНИЕ НАПИТКА» готов. Спасибо за сумму без сдачи! :)"
//  */
// /**
//  * Задание 1.
//  *
//  * Пользователь вводит в модальное окно любое число.
//  *
//  * В консоль вывести сообщение:
//  * - Если число чётное, вывести в консоль сообщение «Ваше число чётное.»;
//  * - Если число не чётное, вывести в консоль сообщение «Ваше число не чётное.»;
//  * - Если пользователь ввёл не число, вывести новое модальное окно с сообщением «Необходимо ввести число!».
//  * - Если пользователь во второй раз ввёл не число, вывести сообщение: «⛔️ Ошибка! Вы ввели не число.».
//  */
//
// let myNumber=+prompt("Введите число")
//
// if(myNumber %2 === 0) {
//     console.log('Ваше число четное');
// }
// else if(myNumber %2 === 1) {
//     console.log('Ваше число не четное')
// }
// else {
//     myNumber= +prompt('Необходимо ввести число!')
//     if (isNaN(myNumber));
//     console.log('Ошибка! Вы ввели не число')
// }
//

//
//
//  for (let i=1; i<=10);
//
// console.log(i);
// i=i+1; //i++


/**
 * Задание 1.
 *
 * С помощью цикла вывести в консоль все нечётные числа,
 * которые находятся в диапазоне от 0 до 300.
 *
 * Заметка:
 * Чётное число — это число, которое делится на два.
 * Нечётное число — это число, которое не делится на два.
 *
 * Продвинутая сложность:
 * Не выводить в консоль числа, которые делятся на 5.
 */

for (let i=0; i<=300; i++){
    if(i%2!==0){
        if(i%5!==0){
            console.log(i)
        }
    }
}
/**
 * Задание 2.
 *
 * Пользователь должен ввести два числа.
 * Если введённое значение не является числом,
 * программа продолжает опрашивать пользователя до тех пор,
 * пока он не введёт число.
 *
 * Когда пользователь введёт два числа, вывести в консоль сообщение:
 * «Поздравляем. Введённые вами числа: «ПЕРВОЕ_ЧИСЛО» и «ВТОРОЕ_ЧИСЛО».».
 */
//Первый вариант
// let first
// let second
// do{
//      first =+prompt("Enter the first number");
//      second=+prompt("Enter the second number");
// }
// while( isNaN(first)|| isNaN(second));
// alert(`Поздравляем. Введённые вами числа: ${first} и ${second}`)
//     //второрй вариант
// let first;
// let second;
// do {
//     first = +prompt("Enter the first number");
// } while (isNaN(first));
// do {
//     second = +prompt("Enter the second number");
// } while (isNaN(second));
// alert(`Вітаю! Ведені вами числа: ${first} та ${second}.`);
/**
 * Задание 3.
 *
 * Написать программу, которая будет опрашивать и приветствовать пользователя.
 *
 * Программа должна узнать у пользователя его:
 * - Имя;
 * - Фамилию;
 * - Год рождения.
 *
 * Если пользователь вводит некорректные данные,
 * программа должна повторно опрашивать его до тех пор,
 * пока данные не будут введены корректно.
 *
 * Данные считается введёнными некорректно, если:
 * - Пользователь не вводит в поле никаких данных;
 * - Год рождения, введённый пользователем меньше, чем 1910 или больше, чем текущий год.
 *
 * Когда пользователь введёт все необходимые данные корректно,
 * вывести в консоль сообщение:
 * «Добро пожаловать, родившийся в ГОД_РОЖДЕНИЯ, ИМЯ ФАМИЛИЯ.».
 */

// let userName;
// let userSurname;
// let birthDate;
//
// do {
//      userName = prompt("Enter your name");
// } while(userName === '' || userName === null);
//
// do {
//     userSurname = prompt("Enter your surname");
// } while(userSurname === '' || userSurname === null);
// do{
//     birthDate = prompt('Enter your year of birth');
// }while (isNaN(birthDate) || birthDate === 0 && birthDate < 1990 || birthDate > 2021);
//
// console.log(`Добро пожаловать, родившийся в ${birthDate}, ${userName}, ${userSurname}`);


/**
 * Задание 4.
 *
 * Написать программу-помощник преподавателя.
 *
 * Будем использовать американскую систему оценивания знаний.
 * Эта система работает на баллах и оценках в виде букв.
 * Расшифровывается следующим образом:
 *
 * Баллы  | Оценка |
 * 95-100 | A      |
 * 90-94  | A-     |
 * 85-89  | B+     |
 * 80-84  | B      |
 * 75-79  | B-     |
 * 70-74  | C+     |
 * 65-69  | C      |
 * 60-64  | C-     |
 * 55-59  | D+     |
 * 50-54  | D      |
 * 25-49  | E      |
 * 0-24   | F      |
 * -----------------
 *
 * Программа должна спрашивать имя и фамилию студента, а также количество баллов, которое на набрал.
 *
 * Программа должна повторно запрашивать данные, если были некорректно введены:
 * - Имя студента (строка, состоящая минимум из двух слов);
 * - Количество баллов, которое набрал студент (число от 0 до 100).
 *
 * Если все данные данные введены верно, программа конвертирует
 * числовое количество баллов в буквенную оценку и выводит сообщение в консоль:
 * «К студенту ИМЯ_СТУДЕНТА прикреплена оценка «ОЦЕНКА».».
 *
 * После выведения этого сообщения программа должна спросить,
 * есть-ли необходимость сконвертировать оценку для ещё одного студента,
 * и должна начинать свою работу сначала до тех пор, пока пользователь не ответит «Нет.».
 *
 * Когда пользователь откажется продолжать работу программы, программа выводит сообщение:
 * «✅ Работа завершена.».
 */

let fullname = null;
let mark = null;
let markLetter = null;
do{
    fullname = prompt("Введите имя_фамилию")}
while(fullname === null || fullname.split(' ').length < 2);
do{
    mark = +prompt("Введите балы")
} while (isNaN(mark) || mark < 0 || mark >100);
if (mark < 25){
    markLetter = 'F';
} else if (mark < 50) {
    markLetter = 'E';
} else if (mark < 55){
    markLetter = 'D';
} else if (mark < 60){
    markLetter = 'D+';
}else if (mark < 65){
    markLetter = 'C-';
}else if (mark < 70){
    markLetter = 'C';
}else if (mark < 75){
    markLetter = 'C+'
}else if (mark < 80){
    markLetter = 'B-'
}else if (mark < 85){
    markLetter = 'B'
}else if (mark < 90){
    markLetter = 'B+'
}else if (mark < 95){
    markLetter = 'A-'
}else{
    markLetter = 'A'
}
console.log(fullname, markLetter);
console.log(`К студенту $(fu)`)
