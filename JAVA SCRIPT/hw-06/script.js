
let elements = ['hello', 'world', 23, '23', null]

const filterBy = function (arr,type) {
    const filteredArray = arr.filter(function (elements){
        if (typeof elements !== type){
            return elements
        }
    })
    return filteredArray
}


const numbers = [2,4,6,2]

const result = numbers.map(function (num) {

    return num*num
})

result.forEach(function (num) {
    if (num < 10) console.log(num)
})