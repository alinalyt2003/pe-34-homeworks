const btn = document.querySelector('.navigation-container-button')
const li = document.querySelector('.button-text')
if(localStorage.getItem('style')){
    li.className = localStorage.getItem('style')
}

document.addEventListener('click',(event)=>{
    li.classList.toggle('blue')
    console.log(li.classList.value)
    localStorage.setItem('style', li.classList.value)
})
