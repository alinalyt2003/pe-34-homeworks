const elem = document.querySelectorAll('.btn')
document.addEventListener('keyup', (event)=>{
    console.log(event)
    elem.forEach(el=>{
        if(el.textContent === event.code.slice(-1) || el.textContent === event.code){
            const active = document.querySelector('.btn-blue')
            if (active !== null){
                active.classList.remove('btn-blue')
            }
            el.classList.add('btn-blue')
        }
    })
})
console.log(elem)