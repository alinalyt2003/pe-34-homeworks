const $arrow= $('#arrow')
$(document).on('click','a[href^="#"]',function (event){
    event.preventDefault()
    $('html, body').animate({
        scrollTop: $($.attr(this,'href')).offset().top
    }, 1500)
})
$arrow.on("click", function (){
    $('html, body').animate({
        scrollTop: 0
    }, 1500)
})
$(document).on('scroll', function (){
    if(window.innerHeight< window.scrollY){
        $arrow.show()
    }else{
        $arrow.hide()
    }
})
$('#hide-content').on('click', function (){
    $('#top-rated').slideToggle('slow')
})