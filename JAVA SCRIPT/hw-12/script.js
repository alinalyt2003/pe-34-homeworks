const wrap = document.querySelector('.img-wrapper')
const button = document.getElementById('stop')
const secondButton = document.getElementById('resume')
const span = document.getElementById('timer')
const timeNext = 3000
let interval = createInterval()
let currentIndex = 0
let leftToNext = timeNext

secondButton.addEventListener('click',()=>{
    interval = createInterval()

})
button.addEventListener('click',()=>{
    clearInterval(interval)
} )
function createInterval(){
    return setInterval(()=>{
        leftToNext -=10;
        if(leftToNext <=0){
            leftToNext = timeNext
            changeImage()
        }
        span.textContent = (leftToNext/1000).toFixed(3)
    }, 10)
}

function changeImage(){
    wrap.children[currentIndex].hidden = true;
    currentIndex++
    if(currentIndex === wrap.children.length){
        currentIndex = 0
    }
    wrap.children[currentIndex].hidden = false;
}
let timerId = setTimeout (changeImage,3000)
alert(timerId)
