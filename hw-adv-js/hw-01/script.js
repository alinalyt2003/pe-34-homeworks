// Реализовать класс Employee, в котором будут следующие
// свойства - name (имя), age (возраст), salary (зарплата).
// Сделайте так, чтобы эти свойства заполнялись при создании объекта.
//     Создайте геттеры и сеттеры для этих свойств.
//     Создайте класс Programmer, который будет наследоваться от
//     класса Employee, и у которого будет свойство lang (список языков).
// Для класса Programmer перезапишите геттер для свойства salary.
// Пусть он возвращает свойство salary, умноженное на 3.
// Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;

    }

    get salary() {
        return this._salary;

    }

     set name(name){
         this.name = name;
    }
    set age(age){
        this.age = age;
     }
     set salary(salary){
          this.salary = salary;
     }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
        this._salary=salary
    }
    get salary(){
        return this._salary * 3;
    }
}

const employee = new Employee('Alina', '18', '1000');
const programmer = new Programmer('Ben', ' 23','2000','english');
console.log(employee, programmer);