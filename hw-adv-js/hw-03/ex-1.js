// Задание 1
// Две компании решили объединиться, и для этого им нужно объединить базу данных своих клиентов.
//     У вас есть 2 массива строк, в каждом из них - фамилии клиентов. Создайте на их основе один массив,
//     который будет представлять собой объединение двух массив без повторяющихся фамилий клиентов.
//     const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
// const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
// const newArray = [...clients1, ...clients2]
//
// const filteredStrings = newArray.filter((item, index) => {
//
//
//     return newArray.indexOf(item) === index;
//
// });
// console.log(filteredStrings);
//


// Задание 2
// Перед вами массив characters, состоящий из объектов. Каждый объект описывает одного персонажа.
//     Создайте на его основе массив charactersShortInfo, состоящий из объектов, в которых есть только 3 поля - name, lastName и age.
// const characters = [
//     {
//         name: "Елена",
//         lastName: "Гилберт",
//         age: 17,
//         gender: "woman",
//         status: "human"
//     },
//     {
//         name: "Кэролайн",
//         lastName: "Форбс",
//         age: 17,
//         gender: "woman",
//         status: "human"
//     },
//     {
//         name: "Аларик",
//         lastName: "Зальцман",
//         age: 31,
//         gender: "man",
//         status: "human"
//     },
//     {
//         name: "Дэймон",
//         lastName: "Сальваторе",
//         age: 156,
//         gender: "man",
//         status: "vampire"
//     },
//     {
//         name: "Ребекка",
//         lastName: "Майклсон",
//         age: 1089,
//         gender: "woman",
//         status: "vempire"
//     },
//     {
//         name: "Клаус",
//         lastName: "Майклсон",
//         age: 1093,
//         gender: "man",
//         status: "vampire"
//     }
// ];
//
// let charactersShortInfo = (name, lastName, age) => [{
//     name,
//     lastName,
//     age,
// }]
// characters.forEach((e)=>{
//     console.log(charactersShortInfo(e))
// })



